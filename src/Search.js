import React, { Component } from 'react';
import Autocomplete from './Autocomplete.js';

const Search = ({query, userInputError, changeSearch, onClickSearch, autocompleteData, selectRepo, mayClear, onClear, autocompleteHidden}) => {

  if(userInputError === true) {
    return (
      <div className="input-group with-error">
        <input type="text" onChange={changeSearch} value={query} />
        {(mayClear ? <button disabled>Clear</button> : <button disabled>Search</button>)}
        <span className="error">Invalid string format</span>
      </div>
    )
  } else {
    return (
      <div className="input-group">
        <input type="text" onChange={changeSearch} value={query}/>
        {(mayClear ? <button onClick={onClear}>Clear</button> : <button onClick={onClickSearch}>Search</button>)}
        <span className="error">{" "}</span>
        {( (!autocompleteHidden) && (<Autocomplete
          data={autocompleteData}
          selectRepo={selectRepo}
        />))}
      </div>
    )
  }
}

export default Search;
