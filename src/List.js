import React, { Component } from 'react';

class List extends Component {
  render() {
    if(this.props.data === null) {
      return null;
    }

    if(this.props.data.length === 0 && this.props.selectedPage === 1) {
      return <div className="list empty">This repository has no issues</div>;
    }

    function renderPaging() {
      return (<div className="paging"><div className="per-page">
                <span className="caption">Per Page:</span>
                <button className={(this.props.selectedPerPage === 20 ? "selected" : "")} onClick={(e) => this.props.selectPerPage(20)} disabled={(this.props.isLoading)}>20</button>
                <button className={(this.props.selectedPerPage === 50 ? "selected" : "")} onClick={(e) => this.props.selectPerPage(50)} disabled={(this.props.isLoading)}>50</button>
                <button className={(this.props.selectedPerPage === 100 ? "selected" : "")} onClick={(e) => this.props.selectPerPage(100)} disabled={(this.props.isLoading)}>100</button>
              </div>
              <div className="page-navigation">
                <button onClick={this.props.pageBack} disabled={(this.props.isLoading || this.props.selectedPage === 1)}>← Back</button>
                <button onClick={this.props.pageNext} disabled={(this.props.isLoading || this.props.data.length === 0)}>Next →</button>
              </div></div>);
    };


    function renderData() {
      if(this.props.data.length === 0) {
        return <span>You have reached the end of list</span>;
      }

      return (<ul className={this.props.className}>
                {this.props.data.map((item) => (<li key={item.id}>
                  {this.props.displayFunc(item)}
                </li>))}
              </ul>);
    }

    return (
          <div className="list">
            {(this.props.isLoading) && (<div className="overlay"></div>)}
            {renderPaging.bind(this)()}
            {renderData.bind(this)()}
            {renderPaging.bind(this)()}
          </div>)
  }
}

export default List;
