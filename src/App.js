import React, { Component } from 'react';
import './App.css';
import List from './List.js';
import Search from './Search.js';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'



class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      query: "",
      issues: null,
      page: 1,
      per_page: 20,
      loadingIssues: false,
      userInputError: false,
      serverError: false,
      serverErrorMessage: "",
      username: "",
      reponame: "",
      autocomplete: [],
      autocompleteHidden: false
    };

    this.changeSearch = this.changeSearch.bind(this);
    this.fetchIssues = this.fetchIssues.bind(this);
    this.onClickFindIssues = this.onClickFindIssues.bind(this);
    this.selectRepo = this.selectRepo.bind(this);
    this.fetchReposAutocomplete = this.fetchReposAutocomplete.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.hideAutocomplete = this.hideAutocomplete.bind(this);
  }

  onClickFindIssues() {
    var args = null;

    if(this.state.query !== null) {
      args = this.state.query.split(' ');
    }

    if(args.length !== 2) {
      this.setState(Object.assign({}, this.state, {
        userInputError: true
      }));

      return true;
    }

    var [username, reponame] = args;

    this.state = Object.assign({}, this.state, {
      page: 1,
      per_page: 20,
      username: username,
      reponame: reponame
    });

    this.fetchIssues();
  }

  fetchIssues() {

    this.setState(Object.assign({}, this.state, {
      userInputError: false,
      loadingIssues: true,
      serverError: false
    }));

    fetch(
        "https://api.github.com/repos/"
        + this.state.username
        + "/"
        + this.state.reponame
        + "/issues?page="
        + this.state.page
        + "&state=all&per_page="
        + this.state.per_page)
      .then(blob => blob.json())
      .then(json => {
        if(json.message !== undefined) {
          this.setState(Object.assign({}, this.state, {
            serverError: true,
            serverErrorMessage: json.message,
            loadingIssues: false
          }));
        } else {
          this.setState(Object.assign({}, this.state, { issues: json, loadingIssues: false }));
        }
      });
  }

  fetchReposAutocomplete(username, reponame = null) {

    if(reponame !== null) {
      fetch(
          "https://api.github.com/search/repositories?q="
          + "user:" + username
          + "+" + reponame + "+in:name+fork:true"
          )
        .then(blob => blob.json())
        .then(json => {
          if(json.items !== undefined) {
            this.setState(Object.assign({}, this.state, {
              autocomplete: json.items.map((item) => {
              return {
                  username: item.owner.login,
                  reponame: item.name,
                  url: item.url
              };
            })}));
          }
        });
    } else {
      fetch(
          "https://api.github.com/users/"
          + username
          + "/repos")
        .then(blob => blob.json())
        .then(json => {
          if(json.message !== undefined) {
          } else {
            this.setState(Object.assign({}, this.state, {
              autocomplete: json.map((item) => {
              return {
                  username: item.owner.login,
                  reponame: item.name,
                  url: item.url
              };
            })}));
          }
        });
    }

  }

  changeSearch(e) {

    if(e.target.value.trim() === "") {
      this.setState(Object.assign({}, this.state, {
        autocomplete: [],
        query: "",
        issues: null,
        userInputError: false,
        serverError: false,
        serverErrorMessage: ""
      }));
      return null;
    }

    this.setState(Object.assign({}, this.state, {
      userInputError: false
    }));

    var regUser = new RegExp(/\w+\s/i);
    var regUserRepo = new RegExp(/\w+\s\w+/i);

    if(e.target.value.match(regUserRepo)) {
      var [username, reponame] = e.target.value.split(/\s+/)
      this.fetchReposAutocomplete(username, reponame);
    }else if(e.target.value.match(regUser)) {
      this.fetchReposAutocomplete(e.target.value.trim());
    }



    this.setState(Object.assign({}, this.state,{
      query: e.target.value,
      autocompleteHidden: false
    }));
  }

  selectPage(p) {
    this.state = Object.assign({}, this.state,{
      page: p
    });

    this.fetchIssues();
  }

  selectPerPage(pp) {
    this.state = Object.assign({}, this.state,{
      per_page: pp,
      page: 1
    });

    this.fetchIssues();
  }


  selectRepo(username, reponame) {
    this.state = Object.assign({}, this.state, {
      query: username + " " + reponame,
      username: username,
      reponame: reponame,
      page: 1,
      per_page: 20,
      autocomplete: []
    });

    this.fetchIssues();
  }

  clearSearch() {
    this.state = Object.assign({}, this.state, {
      query: "",
      username: "",
      reponame: "",
      autocomplete: [],
      issues: null
    });
  }

  hideAutocomplete() {
    this.setState(Object.assign({}, this.state, {
      autocompleteHidden: true
    }));
  }

  renderMain() {
    return (
      <div className="main">
        <div className="header" onClick={this.hideAutocomplete}>
          <Search
              query={this.state.query}
              userInputError={this.state.userInputError}
              changeSearch={this.changeSearch}
              onClickSearch={this.onClickFindIssues}
              autocompleteData={this.state.autocomplete}
              selectRepo={this.selectRepo}
              mayClear={(this.state.username !== "" && this.state.reponame !== "")}
              onClear={this.clearSearch}
              autocompleteHidden={this.state.autocompleteHidden}
            />
        </div>
        <div className="content">
          {(this.state.serverError === true
            ? (<div className="left-col error-message">{this.state.serverErrorMessage}</div>)
            : (<div className="left-col">
                <List
                    className="issues-list"
                    data={this.state.issues}
                    isLoading={(this.state.loadingIssues === true)}
                    selectPerPage={(p) => this.selectPerPage(p)}
                    pageNext={() => this.selectPage(this.state.page + 1)}
                    pageBack={() => this.selectPage(this.state.page - 1)}
                    selectedPerPage={this.state.per_page}
                    selectedPage={this.state.page}
                    displayFunc={(item) => {
                      var dt = new Date(Date.parse(item.created_at));
                      return (
                        <div className="issue-item">
                          <Link to={`/issue/${item.id}`}>
                            <span className="number">{"#"+item.number}</span>
                          </Link>
                          <span className="title">{item.title}</span>
                          <span className="date">{dt.toLocaleString('ru-RU', { hour12: false })}</span>
                        </div>
                      )
                    }}
                  />
              </div>
            ))}
        </div>
      </div>
    );
  }

  render() {
    return (
      <Router>
        <div className="wrapper">
          <Route exact path={'/issue/:issueId'} render={(e) => {
            if(this.state.issues === null) {
                return <Redirect to={{ pathname: '/'}}/>;
            }
            var issueId = e.match.params.issueId;
            var issue = this.state.issues.reduce(function(acc, item) {
              if(acc !== null) {
                return acc;
              }
              if(item.id === parseInt(issueId)) {
                acc = item;
              }
              return acc;
            }, null);

            if(issue !== null) {
              return (
                <div className="issue-details">
                  <Link to={`/`} className="close-btn">
                    <span className="close">← Go back</span>
                  </Link>
                  <div className="user-info">
                    <img src={issue.user.avatar_url} />
                    <a href={issue.user.html_url} className="username">{issue.user.login}</a>
                  </div>
                  <div className="issue-info">
                    <span className="title">{issue.title}</span>
                    <div className="body">
                        <pre>{issue.body}</pre>
                    </div>
                    <div className="status">
                      {"State: " + issue.state}
                    </div>
                  </div>
                </div>
              )
            }
            return null;
          }} />

          <Route exact path={'/'} render={(e) => {
            return this.renderMain();
          }} />
        </div>
      </Router>
    );
  }
}

export default App;
