import React, { Component } from 'react';

const Autocomplete = ({data, selectRepo}) => {
  if(data.length > 0) {
    return (
      <ul className="autocomplete">
        {data.map((item, ind) => {
          return (<li key={ind} onClick={() => selectRepo(item.username, item.reponame)}>
                    {item.username + "/" + item.reponame}
                  </li>);
        })}
      </ul>
    )
  }

  return null;
}

export default Autocomplete;
